%module launch

%include "launch.h"

%{
  #include "launch.h"
%}

typedef long long int MPI_Comm;

typedef long long int MPI_Info;
