#!/bin/sh
set -eu

THIS=$( dirname $0 )
# export SWIFT_PATH=$THIS

MACHINE=${MACHINE:-}

stc -u -r $PWD test.swift
turbine -n 4 $MACHINE test.tic
# -e SWIFT_PATH
