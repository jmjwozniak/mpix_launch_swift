#!/bin/bash
set -eu

# SUBMIT
# Submit workflow.x
# For Swan

export NODES=4 # Default
while getopts "n:" OPT
do
  case $OPT
  in
    n) NODES=$OPTARG ;;
    *) exit 1 ;;
  esac
done

export THIS=$( readlink --canonicalize-existing $( dirname $0 ) )

export PPN=1
export WALLTIME=00:03:00
export OUTPUT_FILE=$( mktemp $THIS/out-XXX --suffix .txt )
export ARGS="${*}"

m4 $THIS/job.sh.m4 > $THIS/job.sh
chmod u+x $THIS/job.sh

echo OUTPUT_FILE=$OUTPUT_FILE
qsub $THIS/job.sh
