changecom(`dnl')#!/bin/bash -l
# We use changecom to change the M4 comment to d-n-l, not hash

define(`getenv', `esyscmd(printf -- "$`$1'")')

ifelse(getenv(PROJECT), `',,
#PBS -A getenv(PROJECT)
)
ifelse(getenv(QUEUE), `',,
#PBS -q getenv(QUEUE)
)
#PBS -l walltime=getenv(WALLTIME)
#PBS -o getenv(OUTPUT_FILE)
# Merge stdout/stderr
#PBS -j oe

### Default aprun mode
#PBS -l nodes=getenv(NODES):ppn=getenv(PPN)

set -eu

echo
printf "START: "
date "+%Y-%m-%d %H:%M %p"

THIS=getenv(THIS)
ARGS="getenv(ARGS)"
DIAMOND=$( readlink --canonicalize-existing $THIS/../.. )

# Set PATH for child.x
PATH=$DIAMOND:$PATH

cd $THIS

CODE=0
echo aprun ...
if ! aprun --batch-args $DIAMOND/workflow.x $ARGS
# child.x 1010 /dev/null /dev/null
then
  CODE=$?
fi
echo "aprun exit code: $CODE"

echo
printf "STOP:  "
date "+%Y-%m-%d %H:%M %p"
echo
