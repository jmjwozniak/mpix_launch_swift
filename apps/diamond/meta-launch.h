
/**
   META LAUNCH
   Choose which version of launch to use
*/

// Default meta API
int launch(const char* cmd, char** argv, MPI_Info info, int root,
           MPI_Comm comm, int* exit_code);

#ifndef CRAY // ANL version

#include <MPIX_Comm_launch.h>

int
launch(const char* cmd, char** argv, MPI_Info info, int root,
       MPI_Comm comm, int* exit_code)
{
  return MPIX_Comm_launch(cmd, argv, info, root, comm, exit_code);
}

#else // Cray version

int MPIX_Comm_launch(const char* cmd, int argc, char** argv,
                     MPI_Comm comm);

int
launch(const char* cmd, char** argv, MPI_Info info, int root,
       MPI_Comm comm, int* exit_code)
{
  // Count args
  int argc = 0;
  while (argv[++argc] != NULL) { }
  MPIX_Comm_launch(cmd, argc, argv, comm);
  *exit_code = 0; // Exit code is not yet returned on Cray
  return MPI_SUCCESS;
}

#endif
