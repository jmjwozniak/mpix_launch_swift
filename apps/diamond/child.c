
/**
   CHILD
   A simple stand-alone MPI program
   Can be called from workflows
   See usage() for usage
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mpi.h>

int mpi_rank, mpi_size;

#include "functions.h"

static void
usage(void)
{
  printf("child: requires task ID, input filename, output filename!\n");
}

int
main(int argc, char* argv[])
{
  MPI_Init(NULL, NULL);
  int mpi_rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if (argc != 4)
  {
    if (mpi_rank == 0)
      usage();
    return EXIT_FAILURE;
  }
  /** A task ID */
  int id = strtol(argv[1], NULL, 10);
  char* input  = argv[2];
  char* output = argv[3];

  if (mpi_rank == 0)
    printf("child: starting: ID: %i size: %i output: %s\n",
           id, size, output);

  FILE* fp;
  fp = fopen(input, "r");
  check(fp != NULL, "child: ID: %i: Could not read from: %s\n",
        id, input);
  // No need to actually read
  fclose(fp);

  fp = fopen(output, "w");
  check(fp != NULL, "child: ID: %i: Could not write to: %s\n",
        id, output);
  fprintf(fp, "%i\n", id);
  fclose(fp);
  sleep(id%4); // Sleep for up to 3 seconds

  if (mpi_rank == 0)
    printf("child: complete: ID: %i\n", id);

  MPI_Finalize();
  return EXIT_SUCCESS;
}
