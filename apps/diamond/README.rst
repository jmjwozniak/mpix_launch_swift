
Diamond example
===============

Runs a workflow that looks like this::

    A
  / | \
 B  B  B  . . .
  \ | /
    C

There are two implementations: one in C, one in Swift/T.

Note that all references to MPIX_Comm_launch() have been replaced with a
call to launch(), which either uses the ANL version or the Cray
version as selected in meta-launch.h .

Checkout
--------

::

   $ gut clone git@bitbucket.org:jmjwozniak/mpix_launch_swift.git
   $ cd mpix_launch_swift
   $ git submodule init
   $ git submodule update

Local machine or login node
---------------------------

This is set up on the Swan login node::

  # Swan: Add Swift/T and MPICH to PATH:
  $ PATH=/home/users/p02473/Public/sfw/local/swift-t/2018-11-04/stc/bin:$PATH
  $ PATH=/home/users/p02473/Public/sfw/mpich-3.2.1/bin:$PATH

See below if you want to build Swift/T on a local machine.

Plain MPI/C
~~~~~~~~~~~

::

  $ make
  # Runs 4 processes total, child jobs of size 2
  $ mpiexec -n 4 ./workflow.x ./child.x 2

See workflow.c for usage of launch()

Swift/T
~~~~~~~

::

  $ make child.x
  $ swift-t -n 4 workflow.swift ./child.x 2

Swan compute nodes
------------------

::

  # Swan: Add Swift/T and PrgEnv to PATH:
  $ PATH=/home/users/p02473/Public/sfw/compute/swift-t/2018-11-13/stc/bin:$PATH
  $ module load PrgEnv-gnu

Plain MPI/C
~~~~~~~~~~~

::

   $ module load PrgEnv-gnu
   $ make CRAY=1
   # Submit with same arguments:
   $ submit/swan/run.sh $PWD/child.x 2


Swift/T
~~~~~~~

::

   $ make CRAY=1 child.x
   # Submit with same arguments:
   $ swift-t -m cray -n 4 $PWD/child.x 2


Swift/T build instructions
--------------------------

Set up Java: ::

  $ module load java

Add SWIG to PATH: ::

  $ PATH=/home/users/p02473/Public/sfw/swig-3.0.12:$PATH

Local
~~~~~

Put MPICH in PATH: ::

  $ PATH=/home/users/p02473/Public/sfw/mpich-3.2.1/bin:$PATH

Clone/build: ::

  $ git clone git@github.com:swift-lang/swift-t.git
  $ cd swift-t
  $ dev/build/init-settings.sh

Edit dev/build/swift-t-settings.sh to set SWIFT_T_PREFIX and CC, then: ::

  $ dev/build/build-swift-t.sh

Swan compute
~~~~~~~~~~~~

::

  $ git clone git@github.com:swift-lang/swift-t.git
  $ cd swift-t
  $ dev/build/init-settings.sh

*  Edit dev/build/swift-t-settings.sh
#. Set SWIFT_T_PREFIX
#. Uncomment the Swan section at the end, then:

::

  $ dev/build/build-swift-t.sh
