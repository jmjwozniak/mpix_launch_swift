
#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>

/** @return true=success, false=fail */
__attribute__ ((unused))
static bool
parse_int(const char* s, int* result)
{
  char* t;
  *result = strtol(s, &t, 10);
  if (s == t)
    return false;
  return true;
}

static void
check(bool condition, char* fmt, ...)
{
  if (! condition)
  {
    if (mpi_rank == 0)
    {
      va_list ap;
      va_start(ap, fmt);
      vprintf(fmt, ap);
      va_end(ap);
    }
    exit(EXIT_FAILURE);
  }
}
