
/**
   WORKFLOW SWIFT
   Simply provide a child program and width argument for the size
   of the child jobs
*/

import assert;
import launch;
import sys;
import io;
import unix;

// These variables have the same conventions as in workflow.c:
child = argp(1);
// Maximal number of concurrent MPI jobs:
width = string2int(argp(2));
// Turbine workers is the number of MPI ranks being used for Swift/T work:
procs = turbine_workers() %/ width;

// Sanity check for existence of child
output, exit_code = system1("which " + child);
if (exit_code != 0)
{
  assert(false, "Could not find child: " + child);
}
else
{
  printf("child: " + output);
}

// Define a file variable
file A<"A.txt">;

// Set up the arguments to the task A
string args_A[] = [ "1", "/dev/null", filename(A) ];
// Run task A, closing file A
@par=procs launch(child, args_A) => A = touch();

// Define an array of file variables
file B[];
A => {
  foreach i in [0:width-1] {
    file B_i<"B-%i.txt"%i>;
    string args_B[] = [ int2string(i+10), filename(A), filename(B_i) ];
    @par=procs launch(child, args_B) => B_i = touch();
    B[i] = B_i;
  }
}

file C<"C.txt">;
string args_C[] = [ "1000", filename(B[0]), filename(C) ];
// The following statement fires when all entries in B are set:
wait deep (B) {
  @par=procs launch(child, args_C) => C = touch();
}
