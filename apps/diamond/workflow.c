
/**
   WORKFLOW C
   Simply provide a child program and width argument for the size
   of the child jobs
*/

#include <stdio.h>
#include <string.h>

#include <mpi.h>

#include "meta-launch.h"

int mpi_rank, mpi_size;

#include "functions.h"

/* The Cray implementation differs from the ANL version on
   the use of argv.  ANL is based on MPI_Comm_spawn(),
   which says "argv is an array of strings containing arguments that
   are passed to the program. The first element of argv is the first
   argument passed to command, not, as is conventional in some
   contexts, the command itself."
   Thus, we use args_offset to switch this.
*/
#ifdef CRAY
const int cray = 1;
#else
const int cray = 0;
#endif

int
main(int argc, char* argv[])
{
  MPI_Init(NULL, NULL);

  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  check(argc == 3, "workflow: requires: <CHILD> <WIDTH>!\n");

  /** Number of concurrent MPI jobs */
  char* child = argv[1];
  int width;
  bool rc = parse_int(argv[2], &width);
  check(rc, "Could not parse integer: '%s'\n", argv[1]);

  if (mpi_rank == 0) printf("workflow: starting...\n");

  int procs = mpi_size/width;
  check(procs*width == mpi_size,
        "MPI size not divisible by width: %i/%i\n", mpi_size, width);

  int args_offset = cray ? 1 : 0;

  char* args[   5];
  char  arg0[1024];
  char  arg1[1024];
  char  arg2[1024];
  char  arg3[1024];
  args[0] = &arg0[0]; // int ID
  args[1] = &arg1[0]; // input file
  args[2] = &arg2[0]; // output file
// Null terminator
  if (cray)
  {
    args[3] = &arg3[0];
    args[4] = NULL;
  }
  else // ANL
  {
    args[3] = NULL;
  }

  int color = mpi_rank/procs;
  MPI_Comm comm;
  MPI_Comm_split(MPI_COMM_WORLD, color, mpi_rank, &comm);

  int comm_rank, comm_size;
  MPI_Comm_rank(comm, &comm_rank);
  MPI_Comm_size(comm, &comm_size);
  printf("%i: %i/%i\n", mpi_rank, comm_rank, comm_size);

  int id, exit_code;

  char test_child[1024];
  sprintf(test_child, "which %s", child);
  int status;
  if (mpi_rank == 0) status = system(test_child);
  check(status == EXIT_SUCCESS, "Could not find child: %s\n", child);

  if (cray)
    strcpy(args[0], child);

  // A
  if (mpi_rank == 0) printf("Phase: A\n");
  if (mpi_rank < procs)
  {
    id = 1;
    sprintf(args[args_offset  ], "%i", id);
    sprintf(args[args_offset+1], "/dev/null");
    sprintf(args[args_offset+2], "A.txt");
    launch(child, args, MPI_INFO_NULL, 0, comm, &exit_code);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  // B
  if (mpi_rank == 0) printf("Phase: B\n");
  id = 10+color;
  sprintf(args[args_offset  ], "%i", id);
  sprintf(args[args_offset+1], "A.txt");
  sprintf(args[args_offset+2], "B-%i.txt", id);
  launch(child, args, MPI_INFO_NULL, 0, comm, &exit_code);

  MPI_Barrier(MPI_COMM_WORLD);

  // C
  if (mpi_rank == 0) printf("Phase: C\n");
  if (mpi_rank < procs)
  {
    id = 1000;
    sprintf(args[args_offset  ], "%i", id);
    sprintf(args[args_offset+1], "B-%i.txt", 10);
    sprintf(args[args_offset+2], "C.txt");
    launch(child, args, MPI_INFO_NULL, 0, comm, &exit_code);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  if (mpi_rank == 0) printf("Complete.\n");

  // Clean up
  MPI_Comm_free(&comm);

  if (mpi_rank == 0)
    printf("workflow: complete.\n");

  MPI_Finalize();
  return EXIT_SUCCESS;
}
