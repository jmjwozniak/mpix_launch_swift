#!/bin/sh
set -eu

# Settings:
export NODES=2
export PPN=3
export WALLTIME=00:01:00
export PROJECT=CI-MCB000119
export QUEUE=batch

export PROCS=$(( NODES*PPN ))

THIS=$( cd $( dirname $0 ) ; /bin/pwd )
cd $THIS

m4 beagle.pbs.m4 > beagle.pbs

qsub beagle.pbs
