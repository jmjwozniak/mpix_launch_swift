#!/bin/sh
set -e

if ! which mpicc > /dev/null
then
  echo "Put mpicc in your PATH!"
  exit 1
fi

make CC=mpicc
