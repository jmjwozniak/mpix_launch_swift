
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>
#include <unistd.h>

#include <mpi.h>

static int mpi_rank, mpi_size;

static void
msg(char* fmt, ...)
{
  static const int MAX_MSG = 128;
  char b[MAX_MSG];
  char* p = &b[0];
  va_list ap;
  va_start(ap, fmt);
  p += sprintf(p, "[%i] ", mpi_rank);
  vsprintf(p, fmt, ap);
  va_end(ap);
  printf("%s\n", b);
}

int
main(int argc, char* argv[])
{
  MPI_Init(0, 0);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  char hostname[HOST_NAME_MAX];
  gethostname(hostname, HOST_NAME_MAX);
  printf("rank: %i host: %s\n", mpi_rank, hostname);
  if (mpi_rank == 0)
  {
    msg("size: %i", mpi_size);
    for (int i = 0; i < argc; i++)
      msg("arg:  %i %s", i, argv[i]);
  }
  MPI_Finalize();
}
