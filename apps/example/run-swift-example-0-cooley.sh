#!/bin/sh
set -eu

THIS=$( dirname $0 )
LAUNCH=$( cd $THIS/../../src ; /bin/pwd )

export MODE=cluster PROJECT=Candle_ECP QUEUE=default
MACHINE="-m cobalt"

MPI_BIN=/home/wozniak/Public/sfw/x86_64/mpich-3.2/bin/mpicc
P=$MPI_BIN:$PATH

set -x
export TURBINE_LOG=1
stc -u -I $LAUNCH -r $LAUNCH example-0.swift
turbine $MACHINE -n 4 -e PATH=$P example-0.tic --this=$PWD
