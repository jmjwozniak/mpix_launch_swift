
import io;
import launch;
import stats;
import string;

/** Return count copies of s concatenated together */
(string result) duplicate(string s, int count)
{
  if (count == 1)
  {
    result = s;
  }
  else
  {
    result = s + duplicate(s, count-1);
  }
}

// Process counts
int procs[] = [ 6, 2, 1, 1, 2 ]; // only used for sum()

// Commands
cmds = split(trim(duplicate("./write.sh ", size(procs))));

// Command line arguments
string a[][];
a[0] = ["hello.txt", "hello1", "hello2"];
a[1] = ["bye1.txt",  "bye1", "bye2", "bye3"];
a[2] = ["bye2.txt",  "bye1", "bye2", "bye3"];
a[3] = ["bye3.txt",  "bye1", "bye2", "bye3"];
a[4] = ["bye4.txt",  "bye1", "bye2", "bye3"];

color_settings = "0,1,2,3,4,5;6,9;7;10;8,11";
int code = @par=sum_integer(procs)
  launch_multi(procs, cmds, a, EMPTY_SS, color_settings);
printf("exit code: %i", code);
