
/*
  WORKFLOW 3
*/

import io;
import launch;
import stats;
import string;

// Process counts
int procs[] = [ 6, 2, 6, 2, 8 ]; // only used for sum()

// Commands
string cmds[];
cmds[0] = "./write.sh";
cmds[1] = "./write.sh";
cmds[2] = "./noop.sh";
cmds[3] = "./write.sh";
cmds[4] = "./noop.sh";

// Command line arguments
string a[][];
a[0] = ["hello.txt", "hello1", "hello2"];
a[1] = ["bye1.txt",  "bye1", "bye2", "bye3"];
a[2] = [ "_" ]; // noop
a[3] = ["bye3.txt",  "bye1", "bye2", "bye3"];
a[4] = ["bye4.txt",  "bye1", "bye2", "bye3"];

string color_settings_array[] = [ " 0- 5", " 6, 7",   // Node 0
                                  " 8-13", "14,15",   // Node 1
                                           "16-23" ]; // Node 2
color_settings = join(color_settings_array, ";");

int code = @par=sum_integer(procs)
  launch_multi(procs, cmds, a, EMPTY_SS, color_settings);
printf("exit code: %i", code);
