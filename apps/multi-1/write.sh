#!/bin/sh

# WRITE.SH
# Write all arguments, plus some special env vars, into given FILE

FILE=$1
shift
echo $* > $FILE
# These may be set by workflow.swift
echo evar1=$evar1 evar2=$evar2 evar3=$evar3 evar4=$evar4 >> $FILE
