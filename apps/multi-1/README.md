
## Custom node color settings

By default, launch() uses the MPI round-robin process layout, and groups each task in a pipeline in order, that is, the lowest ranks run the first task, the next ranks run the next task, and so on, in accordance with the procs argument.

The new color_settings argument allows the programmer to specify the ranks for use with each task in the pipeline.  For example, in workflow-2.swift, if PPN=6, the user would obtain this task layout:

![IMAGE](workflow-2-nodes.png)

As shown, task T0 would have Node 0 to itself, while tasks T1, T2, T3, and T4 would share Node 1.

This is specified with the Swift code:

```
color_settings = "0,1,2,3,4,5;6,9;7;10;8,11";
int code = @par=sum_integer(procs)
  launch_multi(procs, cmds, a, EMPTY_SS, color_settings);
```

The color_settings shown here encodes 5 semicolon-separated phrases, each a comma-separated list of integers.  Each phrase corresponds to a task in the pipeline.  So the first task T0 will run on ranks 0-5, the second task T1 will run on ranks 3,9, and so on.  (Rank-node placement is specified at job launch time, we assume round-robin as shown, although the user could change this if convenient.)  The ranks are within the communicator for the pipeline, so the user can always start from 0 and does not have to consider the all the ranks in the system (no MPI_COMM_WORLD).

**NOTE:**  This syntax is not final!  We should find common cases and provide convenience syntax for typical uses, like placing jobs on the same node, contingent nodes, etc.

## Toward the demo

This figure shows a 4-task pipeline communicating over POSIX shared memory and Flexpath.

![IMAGE](workflow-3-nodes.png)

There are 3 nodes.  The 4 tasks are shown in green boxes.  The simulator is running on ranks 0-5.  A forwarding task on ranks 6,7 extracts data from the simulator memory and forwards it via Flexpath to a buffering task on Node 1.  This node has extra RAM per process as most of its ranks execute a noop task.  A big Viz task runs on all cores of Node 2, consuming data from the buffer over Flexpath, and producing human-readable output.  This layout is specified in workflow-3.swift with:

```
string color_settings_array[] = [ " 0- 5", " 6, 7",   // Node 0
                                  " 8-13", "14,15",   // Node 1
                                           "16-23" ]; // Node 2
color_settings = join(color_settings_array, ";");

int code = @par=sum_integer(procs)
  launch_multi(procs, cmds, a, EMPTY_SS, color_settings);
```

## Figures

https://docs.google.com/drawings/d/1v2VhSjpZiTRNwF71mFNjRFPqzShpXRkm32Rhs6jlhu8

https://docs.google.com/drawings/d/12olrT1Yb5cKI6toCxXkM9Ayj3_gOOI5zzm2Jy_v4m3E
