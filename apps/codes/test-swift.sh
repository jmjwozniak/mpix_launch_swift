#!/bin/sh
set -eu

export LD_LIBRARY_PATH=$HOME/codes-work/test-custom-comm/codes-install/lib:$HOME/codes-work/test-custom-comm/ROSS/install/lib:$HOME/codes-work/install/lib

export PATH=$HOME/codes-work/test-custom-comm/swift-install/stc/bin:$HOME/codes-work/test-custom-comm/swift-install/turbine/bin:$PATH
export SWIFT_PATH=$PWD
swift-t -n 4 test.swift
